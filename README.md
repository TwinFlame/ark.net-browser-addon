# Ark.Net Browser Addon

Tartary's Ark.Net Browser Addon:

We can add that "Overlay" feature to Discord (atleast Web ui) where users can upload Unlimited size files & folders right in the channels. Discord itself would just store a tinly little file with "Globally Unique ID" which finds it's node on the blockchain. But it would rewrite the <HTML> to appear as a Foundry share for anyone viewing it. It would appear in their personal Foundry with share="public" permissions.

Steemit's "Dessenter" uses the same technology, so we already know it's possible, in the web ui. Rewriting Discord.App is possible too, if we had deep integration into Electron. Or could rewrite the WebKit Render() function.

Native app:
We don't even have to modify Official Discord! Just compile our one in Parallel. And switch a couple functions at runtime. Linux has a remarkable feature LD\_Preload which enables that. http://www.goldsborough.me/c/low-level/kernel/2016/08/29/16-48-53-the_-ld_preload-_trick/
Caveat: This trick would only work in linux.